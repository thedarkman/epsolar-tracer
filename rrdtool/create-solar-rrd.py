#!/usr/bin/python
import rrdtool

rrd = "%EPSOLAR_RRD_LOCATION%"

rrdtool.create(
    rrd,
    "--start", "now",
    "--step", "90",
    "DS:voltage:GAUGE:120:0:%EPSOLAR_MAX_VOLTAGE%",
    "DS:current:GAUGE:120:0:%EPSOLAR_MAX_CURRENT%",
    "DS:power:GAUGE:120:0:%EPSOLAR_MAX_POWER%",
    "DS:load_voltage:GAUGE:120:0:30",
    "DS:load_current:GAUGE:120:0:10",
    "DS:load_power:GAUGE:120:0:300",
    "DS:bat_voltage:GAUGE:120:0:40",
    "DS:bat_current:GAUGE:120:0:30",
    "DS:bat_power:GAUGE:120:0:1200",
    "RRA:MIN:0.5:1:1440",
    "RRA:MIN:0.5:10:10080",
    "RRA:MIN:0.5:60:525600",
    "RRA:AVERAGE:0.5:1:1440",
    "RRA:AVERAGE:0.5:10:10080",
    "RRA:AVERAGE:0.5:60:525600",
    "RRA:LAST:0.5:1:1440",
    "RRA:MAX:0.5:1:1440",
    "RRA:MAX:0.5:10:10080",
    "RRA:MAX:0.5:60:525600")