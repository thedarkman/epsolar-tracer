#!/usr/bin/python

import rrdtool
import time

rrd = "%EPSOLAR_RRD_LOCATION%"
img = "%EPSOLAR_IMAGE_LOCATION%"

ret = rrdtool.graph(img + "graph_daily.png",
      "--start", "-1d", "--vertical-label=Spannung",
      "-w 576",
      "-h 150",
      "--slope-mode",
      "DEF:v1={:s}:voltage:LAST".format(rrd),
      "DEF:v2={:s}:load_voltage:LAST".format(rrd),	
      "DEF:v3={:s}:bat_voltage:LAST".format(rrd),	
      "VDEF:v1a=v1,LAST",
      "VDEF:v2a=v2,LAST",
      "VDEF:v3a=v3,LAST",
      "COMMENT:            Aktuell  Minimum  Maximum\\n",
      "LINE1:v1#ff0000:Panel",
      "GPRINT:v1a:    %5.2lf",
      "GPRINT:v1:MIN:  %5.2lf",
      "GPRINT:v1:MAX:  %5.2lf\\n",
      "LINE1:v2#00ff00:Last",
      "GPRINT:v2a:     %5.2lf",
      "GPRINT:v2:MIN:  %5.2lf",
      "GPRINT:v2:MAX:  %5.2lf\\n",
      "LINE1:v3#0000ff:Bat ",
      "GPRINT:v3a:     %5.2lf",
      "GPRINT:v3:MIN:  %5.2lf",
      "GPRINT:v3:MAX:  %5.2lf\\n",
      "COMMENT:Zuletzt aktualisiert\: {:s}\\r".format(time.strftime('%d.%m.%Y %H\:%M'))
)

ret = rrdtool.graph(img + "/graph_daily_current.png",
      "--start", "-1d", "--vertical-label=Strom",
      "-w 576",
      "-h 150",
      "--slope-mode",
      "DEF:v1={:s}:current:LAST".format(rrd),
      "DEF:v2={:s}:load_current:LAST".format(rrd),
      "DEF:v3={:s}:bat_current:LAST".format(rrd),
      "VDEF:v1a=v1,LAST",
      "VDEF:v2a=v2,LAST",
      "VDEF:v3a=v3,LAST",
      "COMMENT:            Aktuell  Maximum\\n",
      "LINE1:v1#ff0000:Panel",
      "GPRINT:v1a:    %5.2lf",
      "GPRINT:v1:MAX:  %5.2lf\\n",
      "LINE1:v2#00ff00:Last",
      "GPRINT:v2a:     %5.2lf",
      "GPRINT:v2:MAX:  %5.2lf\\n",
      "LINE1:v3#0000ff:Bat ",
      "GPRINT:v3a:     %5.2lf",
      "GPRINT:v3:MAX:  %5.2lf\\n",
      "COMMENT:Zuletzt aktualisiert\: {:s}\\r".format(time.strftime('%d.%m.%Y %H\:%M'))
)

ret = rrdtool.graph(img + "graph_daily_power.png",
      "--start", "-1d", "--vertical-label=Leistung",
      "-w 576",
      "-h 150",
      "--slope-mode",
      "DEF:v1={:s}:power:LAST".format(rrd),
      "DEF:v2={:s}:load_power:LAST".format(rrd),
      "DEF:v3={:s}:bat_power:LAST".format(rrd),
      "VDEF:v1a=v1,LAST",
      "VDEF:v2a=v2,LAST",
      "VDEF:v3a=v3,LAST",
      "COMMENT:            Aktuell    Maximum\\n",
      "LINE1:v1#ff0000:Panel",
      "GPRINT:v1a:   %6.2lf",
      "GPRINT:v1:MAX:    %6.2lf\\n",
      "LINE1:v2#00ff00:Last",
      "GPRINT:v2a:    %6.2lf",
      "GPRINT:v2:MAX:    %6.2lf\\n",
      "LINE1:v3#0000ff:Bat ",
      "GPRINT:v3a:    %6.2lf",
      "GPRINT:v3:MAX:    %6.2lf\\n",
      "COMMENT:Zuletzt aktualisiert\: {:s}\\r".format(time.strftime('%d.%m.%Y %H\:%M'))
)

