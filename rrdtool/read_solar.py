#!/usr/bin/python

import json
import rrdtool
import urllib2 as urllib
import time

host = "%EPSOLAR_IP%"
rrd = "%EPSOLAR_RRD_LOCATION%"
key = "%EPSOLAR_SECRET_KEY%"

mapping = {
    "voltage": "3100",
    "current": "3101",
    "power": "3102",
    "load_voltage": "310c",
    "load_current": "310d",
    "load_power": "310e",
    "bat_voltage": "3104",
    "bat_current": "3105",
    "bat_power": "3106"
}


def get_data(data_dict, map_key):
    if mapping[map_key] in data_dict:
        value = '{:.2f}'.format(data_dict[mapping[map_key]])
    else:
        value = 'U'
    return value


try:
    jsonurl = urllib.urlopen('http://{:s}:%EPSOLAR_PORT%/json/live?apiKey={:s}'.format(host, key), timeout=10)
    json_data = json.loads(jsonurl.read())
    # try to get the data
    voltage = get_data(json_data, 'voltage')
    current = get_data(json_data, 'current')
    power = get_data(json_data, 'power')
    load_voltage = get_data(json_data, 'load_voltage')
    load_current = get_data(json_data, 'load_current')
    load_power = get_data(json_data, 'load_power')
    bat_voltage = get_data(json_data, 'bat_voltage')
    bat_current = get_data(json_data, 'bat_current')
    bat_power = get_data(json_data, 'bat_power')
    update_str = 'N:{}:{}:{}:{}:{}:{}:{}:{}:{}'.format(voltage, current, power, load_voltage, load_current, load_power, bat_voltage, bat_current, bat_power)
    print('{:s} {:s}'.format(time.strftime('%Y-%m-%d %H:%M:%S'), update_str))
    ret = rrdtool.update(rrd, update_str)
except urllib.URLError, e:
    print('http request failed: {}'.format(e))
