#!/bin/bash
#
# This script could be called every minute via crontab
# Will restart gunicorn if not running
#

cd %EPSOLAR_HOME%
GUNICORN_RUNNING=`ps xa | grep "gunicorn" | grep -v grep`
if [ -z "$GUNICORN_RUNNING" ]; then
   DATE=`date '+%Y-%m-%d %H:%M:%S'`
   echo "$DATE gunicorn not running, restarting gunicorn" >> /var/log/rc.local.log
   sudo gunicorn -w 4 -b 0.0.0.0:%EPSOLAR_PORT% info:app
fi
