#!/bin/bash

if [ "$(id -u)" == "0" ]; then
    echo "Please do not run the script with sudo or as root"
    echo
    exit 1
fi

DIR=`find ~ -name "epsolar-tracer" -type d`
HOST=`hostname`

if [ -z "$DIR" ]; then
   echo "could not find the project folder 'epsolar-tracer'. Where are you?"
   exit 1
fi

echo "Welcome to the install of the epsolar-tracer stat tool app."
echo "We will now collect some data so this app will run ..."
echo
echo -n "What will be the hostname/ip of the pi? [$HOST] "
read host
if [ -z "$host" ]; then host="$HOST"; fi
echo
echo -n "Where should the RRD be created? [${DIR}/solar.rrd] "
read solar
if [ -z "$solar" ]; then solar="$DIR/solar.rrd"; fi
echo -n "What will be the maximum panel voltage? [30] "
read max_volt
if [ -z "$max_volt" ]; then max_volt="30"; fi
echo -n "What will be the maximum panel current? [10] "
read max_current
if [ -z "$max_current" ]; then max_current="10"; fi
echo -n "What will be the maximum panel power? [300] "
read max_power
if [ -z "$max_power" ]; then max_power="300"; fi
echo
echo -n "Where should the graphs be generated to (must end with /)? [${DIR}/static/solar/] "
read graphs
if [ -z "$graphs" ]; then graphs="${DIR}/static/solar/"; fi
if [ ! -d "$graphs" ]; then mkdir -p $graphs; fi
echo
echo -n "Which port should we use? [80] "
read port
if [ -z "$port" ]; then port="80"; fi
echo
echo -n "Should the server start on boot (change on /etc/rc.local will be done)? [Y/n] "
read rc_local
if [ -z "$rc_local" ]; then rc_local="Y"; fi
echo
echo -n "Should we create the needed crontab entries? [y/N] "
read cron_tab
if [ -z "$cron_tab" ]; then cron_tab="N"; fi
echo
echo
echo -n "We now need an api key for the app [eec7c995-8259-4e53-9806-54f6607c2ade]: "
read api_key
if [ -z "$api_key" ]; then api_key="eec7c995-8259-4e53-9806-54f6607c2ade"; fi
echo
echo

echo "******************************************************************************************************************"
echo "The hostname used for app will be......: $host"
echo "The database will be generated to......: $solar"
echo "    with max volt/ampere/watt..........: $max_volt/$max_current/$max_power"
echo "The graphs will be generated to........: $graphs"
echo "The mini webserver will run with port..: $port"
echo "The api key used for access is.........: $api_key"
echo "******************************************************************************************************************"
echo

sed -i "s|%EPSOLAR_HOME%|$DIR|g" $DIR/run_solar.sh
sed -i "s|%EPSOLAR_PORT%|$port|g" $DIR/run_solar.sh

sed -i "s|%EPSOLAR_RRD_LOCATION%|$solar|g" $DIR/rrdtool/create-solar-rrd.py
sed -i "s|%EPSOLAR_MAX_VOLTAGE%|$max_volt|g" $DIR/rrdtool/create-solar-rrd.py
sed -i "s|%EPSOLAR_MAX_CURRENT%|$max_current|g" $DIR/rrdtool/create-solar-rrd.py
sed -i "s|%EPSOLAR_MAX_POWER%|$max_power|g" $DIR/rrdtool/create-solar-rrd.py

sed -i "s|%EPSOLAR_RRD_LOCATION%|$solar|g" $DIR/rrdtool/create_solar_graph.py
sed -i "s|%EPSOLAR_IMAGE_LOCATION%|$graphs|g" $DIR/rrdtool/create_solar_graph.py

sed -i "s|%EPSOLAR_IP%|$host|g" $DIR/rrdtool/read_solar.py
sed -i "s|%EPSOLAR_PORT%|$port|g" $DIR/rrdtool/read_solar.py
sed -i "s|%EPSOLAR_RRD_LOCATION%|$solar|g" $DIR/rrdtool/read_solar.py
sed -i "s|%EPSOLAR_SECRET_KEY%|$api_key|g" $DIR/rrdtool/read_solar.py
sed -i "s|%EPSOLAR_SECRET_KEY%|$api_key|g" $DIR/info.py

sudo chmod +x $DIR/run_solar.sh

PIP=`which pip`
if [ -z "$PIP" ]; then
    echo "pip could not be found, make sure it is installed via"
    echo "  sudo apt-get install python-pip"
    exit 1
fi

GUNICORN=`which gunicorn`
if [ -z "$GUNICORN" ]; then
    echo "gunicorn could not be found, make sure it is installed via"
    echo "  sudo pip install gunicorn"
    exit 1
fi


MODBUS_INSTALLED=`sudo pip freeze 2> /dev/null | grep "pymodbus"`
if [ -z "$MODBUS_INSTALLED" ]; then
    echo "modbus could not be found, make sure it is installed via"
    echo "  sudo apt-get install python-pymodbus"
    exit 1
fi

if [ ! -f $solar ]; then
    echo "creating the rrd now ..."
    python $DIR/rrdtool/create-solar-rrd.py
    echo "rrd created."
fi

MSG="service can now be run with: sudo ./run_solar.sh"
if [ "$cron_tab" == "Y" -o "$cron_tab" == "y" ]; then
    crontab -l > /tmp/current_crontab
    echo "*/1 * * * * sudo $DIR/run_solar.sh" >> /tmp/current_crontab
    echo "*/1 * * * * sudo python $DIR/rrdtool/read_solar.py >> /dev/null 2>&1" >> /tmp/current_crontab
    echo "*/2 * * * * sudo python $DIR/rrdtool/create_solar_graph.py" >> /tmp/current_crontab
    crontab /tmp/current_crontab
    rm /tmp/current_crontab
    MSG="service will start with next crontab execution"
else
    echo "Please add those lines to your crontab:"
    echo
    echo "* * * * * sudo $DIR/run_solar.sh"
    echo "* * * * * sudo python $DIR/rrdtool/read_solar.py >> /dev/null 2>&1"
    echo "*/2 * * * * sudo python $DIR/rrdtool/create_solar_graph.py"
    echo
fi

echo
echo "install done."
echo "$MSG"
echo
echo "Access the web app via http://$host/"

exit 0
