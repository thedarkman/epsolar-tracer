# epsolar-tracer

This project extends the github project [epsolar-tracer](https://github.com/kasbert/epsolar-tracer) to a small little 
responsive app using python, flask and javascript to get current data from epsolar tracer 
solar charger and store this data in a RoundRobinDatabase (rrd).

Current status
--------------
* Data is collected every minute via cron job
* Graph images are created every two minutes
* Only one day is shown in graph, rrd will store one year
* Currently some language mixes are possible, graphs have german legend

System requirements
-------------------
* rrdtool
* pymodbus
* flask
* optional Webcam with motion

Python Requirements
-------------------
* Flask
* Flask-Cache   
* Gunicorn

Install
-------
* Install requirements
    * `sudo apt-get install python-pip python-flask python-pymodbus rrdtool python-rrdtool`
    * add `git` to the list if you don't have it installed. (raspbian lite does not include it)
        * Check if it's installed with `dpkg -l | egrep "git "`
        * Output should look like this:
            * `ii  git                                   1:2.1.4-2.1+deb8u2                        armhf        fast, scalable, distributed revision control system`
* Check out this project:
    * `git clone https://bitbucket.org/thedarkman/epsolar-tracer.git`
* Change into new directory
    * `cd epsolar-tracer`
* Install Python modules 
    * `sudo pip install -r requirements.txt`
* Start interactive install
    * `./install.sh`

Configuration
-------------
* Port for tracer is set default to `/dev/ttyUSB0`
* Default login / password is `admin: admin`
* credentials could be set via file:
    * Create json config file `config.json` or rename `config.default.json`
* Supported keys: `tracer_port, cache_dir, credentials, camera`
    * `credentials` is json object with `"login": "password"` entries
    * `camera` is json object with `url` and `boundary` parameters
    * see `config.default.json` for more details
* api key must be set (and `install.sh` will set a default one), but can be overwritten in config.json
    * if key is changed while running `install.sh`, all fine. If changed afterwards, change it also in `rrdtool/read_solar.py`

Usage responsive webapp
-----------------------
* Just open this url in browser: `http://<ip-of-raspberry-pi>/`
* Screenshots
    * ![Image of graphs for last day](doc/solar_graphs.png)
    * ![Video of live data](doc/solar_live.gif)

Usage json part
---------------
* Live data could be retrieved via `http://<ip-of-raspberry-pi>/json/live`
    * addition stats or all information data is available with `json/all` or `json/stats`
* Data object with registers is returned
* Register assignment could be look up at [Modbus protocol page](http://www.solar-elektro.cz/data/dokumenty/1733_modbus_protocol.pdf)

