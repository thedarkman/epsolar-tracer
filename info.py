from pyepsolartracer.client import EPsolarTracerClient
from pyepsolartracer.registers import V, A, W, KWH, C, PC, Ton, Register, registers
from pymodbus.client.sync import ModbusSerialClient as ModbusClient

import urllib2
import json
import time
import os
import platform
import tempfile
import logging
import random

from flask import Flask, make_response, Response, request, render_template, session
from flask_cache import Cache
from functools import wraps

if 'Windows' in platform.system():
    tmp_dir = tempfile.gettempdir()
else:
    tmp_dir = '/run'  # new debian and orangepi
    if not os.path.exists(tmp_dir):
        tmp_dir = '/var/run'  # fallback

tracer_port = '/dev/ttyUSB0'
credentials = {}
config = {}
simulate = False
translations = {
    'de': {
        'menu_stats': 'Datenverlauf',
        'menu_live': 'Live Daten',
        'stats_header': 'Graphen',
        'live_data_header': 'Livedaten, refresh alle 5 Sekunden',
        'open_in_popup': 'Live Daten in Pop-Up',
        'panel_voltage': 'Spannung Module',
        'panel_current': 'Strom Module',
        'panel_power': 'Leistung Module',
        'battery_voltage': 'Spannung Batterie',
        'battery_current': 'Strom Batterie',
        'battery_power': 'Leistung Batterie',
        'load_voltage': 'Spannung Last',
        'load_current': 'Strom Last',
        'load_power': 'Leistung Last',
        'battery_max_voltage': 'Max Bat.',
        'battery_min_voltage': 'Min Bat.',
        'battery_temperature': 'Batterie Temp.',
        'battery_soc': 'Batterie SOC',
        'panel_max_voltage': 'Max Module',
        'panel_min_voltage': 'Min Module',
        'live_stream': 'Live video'
    },
    'en': {
        'menu_stats': 'Data stats',
        'menu_live': 'Live data',
        'stats_header': 'Graphs',
        'live_data_header': 'Live data, refresh every 5 seconds',
        'open_in_popup': 'Live data in pop-up',
        'panel_voltage': 'Array Voltage',
        'panel_current': 'Array Current',
        'panel_power': 'Array Power',
        'battery_voltage': 'Battery Voltage',
        'battery_current': 'Battery Current',
        'battery_power': 'Battery Power',
        'load_voltage': 'Load Voltage',
        'load_current': 'Load Current',
        'load_power': 'Load Power',
        'battery_max_voltage': 'Max Bat.',
        'battery_min_voltage': 'Min Bat.',
        'battery_temperature': 'Battery Temp.',
        'battery_soc': 'Battery SOC',
        'panel_max_voltage': 'Max Array',
        'panel_min_voltage': 'Min Array',
        'live_stream': 'Live stream'
    }
}

try:
    config = json.load(open('config.json'))
    if 'cache_dir' in config.keys():
        tmp_dir = config['cache_dir']
    if 'tracer_port' in config.keys():
        tracer_port = config['tracer_port']
    if 'credentials' in config.keys():
        credentials = config['credentials']
    if 'simulator' in config.keys():
        simulate = config['simulator']
except Exception as e:
    print("Error while getting config: " + str(e))

if 'apiKey' not in config.keys():
    config['apiKey'] = '%EPSOLAR_SECRET_KEY%'

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'filesystem', 'CACHE_DIR': tmp_dir})
print 'using cache tmp path: ' + tmp_dir


def check_api_key():
    """This function checks if an api key was used for access
    """
    return 'apiKey' in request.args and request.args.get('apiKey') == config['apiKey']


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    if not credentials.keys():
        return username == 'admin' and password == 'admin'
    else:
        return username in credentials.keys() and password == credentials[username]


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Solar station login required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if check_api_key():
            return f(*args, **kwargs)
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated


filename = tmp_dir + "/read_lock"
try:
    os.remove(filename)
except EnvironmentError:
    print 'nothing to do here, lock file not there'


def aquire_lock():
    while os.path.exists(filename):
        time.sleep(0.1)
    open(filename, 'w')
    log.info('lock file created: {}'.format(filename))


def release_lock():
    os.remove(filename)
    log.info('lock file removed')


live_registers3100 = [
    # Real-time data (read only) input register
    # Charging equipment input voltage
    Register("Charging equipment input voltage",
             0x3100, "Solar charge controller--PV array voltage",
             V, 100),
    # Charging equipment input current
    Register("Charging equipment input current",
             0x3101, "Solar charge controller--PV array current",
             A, 100),
    # Charging equipment input power
    Register("Charging equipment input power",
             0x3102, "Solar charge controller--PV array power",
             W, 100, 2),
    # Charging equipment output voltage
    Register("Charging equipment output voltage",
             0x3104, "Battery voltage",
             V, 100),
    # Charging equipment output current
    Register("Charging equipment output current",
             0x3105, "Battery charging current",
             A, 100),
    # Charging equipment output power
    Register("Charging equipment output power",
             0x3106, "Battery charging power",
             W, 100, 2),
    # Discharging equipment output voltage
    Register("Discharging equipment output voltage",
             0x310C, "Load voltage",
             V, 100),
    # Discharging equipment output current
    Register("Discharging equipment output current",
             0x310D, "Load current",
             A, 100),
    # Discharging equipment output power
    Register("Discharging equipment output power",
             0x310E, "Load power",
             W, 100, 2),
    # Battery Temperature
    Register("Battery Temperature",
             0x3110, "Battery Temperature",
             C, 100),
    # Temperature inside equipment
    Register("Temperature inside equipment",
             0x3111, "Temperature inside case",
             C, 100),
    # Power components temperature
    Register("Power components temperature",
             0x3112, "Heat sink surface temperature of equipments' power components",
             C, 100)
]

live_registers311A = [
    # Battery SOC
    Register("Battery SOC",
             0x311A, "The percentage of battery's remaining capacity",
             PC, 1),
    # Remote battery temperature
    Register("Remote battery temperature",
             0x311B, "The battery tempeture measured by remote temperature sensor",
             C, 100)
]

live_registers311D = [
    # Battery's real rated power
    Register("Battery's real rated power",
             0x311D, "Current system rated votlage. 1200, 2400 represent 12V, 24V",
             V, 100)
]

live_registers3300 = [
    # Maximum input volt (PV) today
    Register("Maximum input volt (PV) today",
             0x3300, "00: 00 Refresh every day",
             V, 100),
    # Minimum input volt (PV) today
    Register("Minimum input volt (PV) today",
             0x3301, "00: 00 Refresh every day",
             V, 100),
    # Maximum battery volt today
    Register("Maximum battery volt today",
             0x3302, "00: 00 Refresh every day",
             V, 100),
    # Minimum battery volt today
    Register("Minimum battery volt today",
             0x3303, "00: 00 Refresh every day",
             V, 100)
]

test_live_registers = [
    # Real-time data (read only) input register
    # Charging equipment input voltage
    Register("Charging equipment input voltage",
             0x3100, "Solar charge controller--PV array voltage",
             V, 100),
    # Charging equipment input current
    Register("Charging equipment input current",
             0x3101, "Solar charge controller--PV array current",
             A, 100)
]

stats_registers = [
    # Maximum input volt (PV) today
    Register("Maximum input volt (PV) today",
             0x3300, "00: 00 Refresh every day",
             V, 100),
    # Minimum input volt (PV) today
    Register("Minimum input volt (PV) today",
             0x3301, "00: 00 Refresh every day",
             V, 100),
    # Maximum battery volt today
    Register("Maximum battery volt today",
             0x3302, "00: 00 Refresh every day",
             V, 100),
    # Minimum battery volt today
    Register("Minimum battery volt today",
             0x3303, "00: 00 Refresh every day",
             V, 100),
    # Consumed energy today
    Register("Consumed energy today",
             0x3304, "00: 00 Clear every day",
             KWH, 100, 2),
    Register("Consumed energy this month",
             0x3306, "00: 00 Clear on the first day of month",
             KWH, 100, 2),
    # Consumed energy this year
    Register("Consumed energy this year",
             0x3308, "00: 00 Clear on 1, Jan.",
             KWH, 100, 2),
    # Total consumed energy
    Register("Total consumed energy",
             0x330A, "Total consumed energy",
             KWH, 100, 2),
    # Generated energy today
    Register("Generated energy today",
             0x330C, "00: 00 Clear every day.",
             KWH, 100, 2),
    # Generated energy this month
    Register("Generated energy this month",
             0x330E, "00: 00 Clear on the first day of month.",
             KWH, 100, 2),
    # Generated energy this year
    Register("Generated energy this year",
             0x3310, "00: 00 Clear on 1, Jan.",
             KWH, 100, 2),
    # Total generated energy
    Register("Total generated energy",
             0x3312, "Total generated energy",
             KWH, 100, 2),
    # Carbon dioxide reduction
    Register("Carbon dioxide reduction",
             0x3314, "Saving 1 Kilowatt=Reduction 0.997KG''Carbon dioxide ''=Reduction 0.272KG''Carton''",
             Ton, 100, 2),
    # Battery Current
    Register("Battery Current",
             0x331B,
             "The net battery current,charging current minus the discharging one. The positive value represents charging and negative, discharging.",
             A, 100, 2),
    # Battery Temp.
    Register("Battery Temp.",
             0x331D, "Battery Temp.",
             C, 100),
    # Ambient Temp.
    Register("Ambient Temp.",
             0x331E, "Ambient Temp.",
             C, 100),
]

# configure the client logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.INFO)


def get_live_data():
    try:
        log.info('method called directly ...')
        aquire_lock()
        if simulate:
            random.seed()
            current = random.randrange(100, 700, 1) / float(100)
            voltage = random.randrange(1300, 5400, 1) / float(100)
            power = round(voltage * current, 2)
            data = {"3303": 12.72, "3300": 14.14, "3302": 12.86, "3104": 12.74, "3105": 0.0, "3106": 0.0,
                    "3100": voltage,
                    "3101": current, "3102": power, "3110": 12.74, "3111": 0.18, "310d": 0.18, "310e": 2.29,
                    "3112": 2.29,
                    "3301": 0.0, "311b": 19.77, "311a": 61, "310c": 12.74}
            time.sleep(1.5)
        else:
            serial_client = ModbusClient(method='rtu', port=tracer_port, baudrate=115200, stopbits=1, bytesize=8,
                                         timeout=1)
            client = EPsolarTracerClient(serialclient=serial_client)
            client.connect()

            data = {}

            response = client.read_multiple_data(live_registers3100)
            for val in response:
                key = '{:x}'.format(val.register.address)
                data[key] = val.value
            time.sleep(0.5)
            response = client.read_multiple_data(live_registers311A)
            for val in response:
                key = '{:x}'.format(val.register.address)
                data[key] = val.value
            time.sleep(0.5)
            response = client.read_multiple_data(live_registers3300)
            for val in response:
                key = '{:x}'.format(val.register.address)
                data[key] = val.value
            client.close()
        data['simulate'] = simulate
        r = make_response(json.dumps(data))
        r.mimetype = 'application/json'
        return r
    finally:
        release_lock()


def get_stats_data():
    try:
        aquire_lock()
        serial_client = ModbusClient(method='rtu', port=tracer_port, baudrate=115200, stopbits=1, bytesize=8, timeout=1)
        client = EPsolarTracerClient(serialclient=serial_client)
        client.connect()

        data = {}

        response = client.read_multiple_data(stats_registers)
        for val in response:
            key = '{:x}'.format(val.register.address)
            data[key] = val.value

        client.close()

        r = make_response(json.dumps(data))
        r.mimetype = 'application/json'
        return r
    finally:
        release_lock()


def render_translated_template(template_name, **kwargs):
    lang_string = request.headers.get('Accept-Language', '')
    first_lang = lang_string.split(',')[0]
    if len(first_lang) == 2:
        language = first_lang
    elif len(first_lang) == 5:
        language = first_lang.split('-')[0]

    if language not in translations.keys():
        language = 'en'
    return render_template(template_name, translations=translations[language], **kwargs)


@app.route("/static/solar/<path:path>")
@cache.cached(timeout=30)
def graphs(path):
    with open(os.path.dirname(os.path.abspath(__file__)) + "/static/solar/" + path, 'rb') as f:
        content = f.read()
        return Response(content, mimetype='image/png')


def is_cam_available():
    return 'camera' in config.keys()


@app.route("/")
@app.route("/index.html")
@requires_auth
def start():
    return render_translated_template('index.html', camera=is_cam_available())


@app.route("/live.html")
@requires_auth
def live():
    return render_translated_template('live.html', camera=is_cam_available())


@app.route("/json/live")
@cache.cached(timeout=2)
@requires_auth
def json_data():
    return get_live_data()


@app.route("/json/stats")
@cache.cached(timeout=5)
@requires_auth
def stats_data():
    return get_stats_data()


@app.route("/cam.html")
@cache.cached(timeout=360)
@requires_auth
def live_cam():
    return render_translated_template('cam.html', camera=is_cam_available())


@app.route("/stream")
@requires_auth
def live_cam_stream():
    try:
        url = config['camera']['url']
        if 'boundary' in config['camera'].keys():
            boundary = config['camera']['boundary']
        else:
            boundary = '--BoundaryString'

        def read_process():
            req = urllib2.urlopen(url)
            for dat in req:
                yield dat

        return Response(read_process(), mimetype='multipart/x-mixed-replace; boundary={:s}'.format(boundary))
    except KeyError:
        r = make_response('{"error": "no cam configured!"}')
        r.mimetype = 'application/json'
        return r

@app.route("/register/<register>")
@requires_auth
def handle_register(register):

    # find register by address
    hex_str = '0x{}'.format(register)
    log.info('register with hex string should be read: {}'.format(hex_str))

    register_name = None
    hex_int = int(hex_str, 16);
    for r in registers:
        if r.address == hex_int:
            register_name = r.name

    if register_name == None:
        r = make_response('{"error": "register not found!"}')
        r.mimetype = 'application/json'
        return r    


    try:
        aquire_lock()
        serial_client = ModbusClient(method='rtu', port=tracer_port, baudrate=115200, stopbits=1, bytesize=8, timeout=1)
        client = EPsolarTracerClient(serialclient=serial_client)
        client.connect()

        data = {}

        response = client.read_input(register_name)
        # instance of Value
        log.info(response)
        data[register] = response.value
        data['registerName'] = response.register.name

        client.close()

        r = make_response(json.dumps(data))
        r.mimetype = 'application/json'
        return r
    finally:
        release_lock()
    
       

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8888, debug=True)
