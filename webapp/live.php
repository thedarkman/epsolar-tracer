<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <link rel="apple-touch-icon" href="img/solar-icon.png">
  <title>Live Daten</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/base.css">
  <link rel="stylesheet" href="css/live.css">
  <script src="js/jquery-3.2.1.min.js" type="application/javascript"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/solar.js" type="text/javascript"></script>
  <script type="application/javascript">
      window.setTimeout(fetchLiveData, 200);
  </script>
</head>
<body>
  <?php
  include('inc/navbar.php');
  ?>
  <h3>Livedaten, refresh alle 5 Sekunden</h3>
  <div id="content">
    <div id="livedata" class="container">
      <form id="liveForm">
        <div class="row">
          <div class="form-group col-xs-2">
            <label for="arrayCurrent">Array Current (A)</label><br>
            <input id="arrayCurrent" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="batteryVoltage">Battery Voltage (V)</label><br>
            <input id="batteryVoltage" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="batteryCurrent">Battery Current (A)</label><br>
            <input id="batteryCurrent" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="loadCurrent">Load Current (A)</label><br>
            <input id="loadCurrent" class="form-control small" type="text" disabled placeholder="---" />
          </div>
        </div>
        <div class="row">
          <div class="form-group col-xs-2">
            <label for="arrayVoltage">Array Voltage (V)</label><br>
            <input id="arrayVoltage" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="batteryMaxVoltage">Max Voltage (V)</label><br>
            <input id="batteryMaxVoltage" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="batteryMinVoltage">Min Voltage (V)</label><br>
            <input id="batteryMinVoltage" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="loadVoltage">Load Voltage (V)</label><br>
            <input id="loadVoltage" class="form-control small" type="text" disabled placeholder="---" />
          </div>
        </div>
        <div class="row">
          <div class="form-group col-xs-2">
            <label for="arrayPower">Array Power (W)</label><br>
            <input id="arrayPower" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="batteryTemp">Battery Temp. (&#8451;)</label><br>
            <input id="batteryTemp" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="batterySoc">Battery SOC (%)</label><br>
            <input id="batterySoc" class="form-control small" type="text" disabled placeholder="---" />
          </div>
          <div class="form-group col-xs-2">
            <label for="loadPower">Load Power (W)</label><br>
            <input id="loadPower" class="form-control small" type="text" disabled placeholder="---" />
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
