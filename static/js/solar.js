var map = {
    'arrayVoltage': '3100',
    'batteryVoltage': '3104',
    'arrayCurrent': '3101',
    'arrayPower': '3102',
    'batteryCurrent': '3105',
    'loadVoltage': '310c',
    'loadCurrent': '310d',
    'loadPower': '310e',
    'batterySoc': '311a',
    'batteryTemp': '311b',
    'batteryMaxVoltage': '3302',
    'batteryMinVoltage': '3303',
    'panelMaxVoltage': '3300',
    'panelMinVoltage': '3301'
};

var valueMap = {};

var seriesData = new Array();
var seriesData2 = new Array();
var maxGraphElements = 50;
var maxValuePower = 0;
var lastId = 0;
var originalName = '';

function fetchLiveData() {
    $.ajax({
        url: 'json/live',
        success: onSuccess,
        error: onError
    });
}

function onSuccess(data) {
    $('#liveForm input').each(function (e) {
        var $this = $(this);
        var id = $this.attr('id');
        var key = map[id];
        var value = getValue(data, key);
        $this.val(value);

        if (id === 'arrayPower' && '---' != value) {
            var size = seriesData.push(value);
            if(value > maxValuePower)
                maxValuePower = value;
            if(size > maxGraphElements)
                seriesData.shift();
            if(chart != undefined) {
                chart.series[0].setData(seriesData, true, {}, false);
                var id = lastId + 1;
                var remove = chart.yAxis[0].plotLinesAndBands[0];
                if(remove !== undefined)
                   chart.yAxis[0].removePlotLine(remove.id);
                var y = maxValuePower <= 15 ? -16 : 16;
                var line = chart.yAxis[0].addPlotLine({ id: id, value: maxValuePower, width: 1, color: 'rgba(0,204,0,0.75)'});
		lastId++;
            }
        }
        
        //if (id === 'arrayVoltage' && '---' != value) {
        //    var size = seriesData2.push(value);
        //    if(size > maxGraphElements)
        //        seriesData2.shift();
        //    if(chart != undefined)
        //        chart.series[1].setData(seriesData2, true, {}, false);
        //}
        if ('---' != value && valueMap[id] != value) {
            $this.addClass('changed');
            valueMap[id] = value;
            window.setTimeout(function () {
                $this.removeClass('changed')
            }, 1000);
        }
    });
    window.setTimeout(fetchLiveData, 5000);
}


function onError(jqXHR, textStatus, errorThrown) {
    var e = errorThrown !== undefined ? errorThrown : textStatus;
    console.log('Error fetching data: '+ e);
    window.setTimeout(fetchLiveData, 5000);
}

function getValue(data, key) {
    if (!!data[key] || (typeof data[key] === 'number')) {
        return data[key];
    } else {
        console.log('key not found: ' + key);
        return '---';
    }
}

